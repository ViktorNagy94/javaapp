
package calculateMyBMI;

import java.util.*;

import javax.imageio.metadata.IIOInvalidTreeException;

public class metricBMI implements BMIcalculator {
	Scanner weightScanner = new Scanner(System.in);
	Scanner heightScanner = new Scanner(System.in);

	double height;
	double weight;

	public metricBMI(double height, double weight) {
		super();
		this.height = height;
		this.weight = weight;
	}
	//This Function calculates the BMI index
	
	public double calculateMyBMI() throws IllegalArgumentException, IllegalFormatException, InputMismatchException {
		System.out.println("Please type your height (e.g.: 165)\n");
		try {
			height = heightScanner.nextDouble();
		} catch (InputMismatchException height) {
			System.out.println("Please Enter valid data(numbers only)!! \n The following exception occured:");
			System.out.println(InputMismatchException.class);
			return 0;
		}
		try {
			if (this.height <= 50 || this.height > 210) {
				throw new IllegalArgumentException();
			}
		} catch (IllegalArgumentException height) {
			System.out.println("Please enter valid data for your height!");
			return 0;
		}
		System.out.println("Please type your weight (e.g.: 60.45)\n");
		try {
			weight = weightScanner.nextDouble();
		} catch (InputMismatchException weight) {
			System.out.println("Please Enter valid data (numbers only)!! \n The following exception occured:");
			System.out.println(InputMismatchException.class);
			return 0;
		}
		try {
			if (this.weight <= 20 || this.height > 200) {
				throw new IllegalArgumentException();
			}
		} catch (IllegalArgumentException height) {
			System.out.println("Please enter valid data for your height!");
			return 0;
		}

		double BMI = weight / (Math.pow(height, 2));
		System.out.println("Your BMI is:");
		return BMI * 10000;
	}
}
